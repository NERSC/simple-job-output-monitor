#!/bin/bash
#SBATCH -C knl
#SBATCH --nodes=1
#SBATCH --time=30
#SBATCH --qos=debug

module load python3
timeout=10 #seconds
filename=test.$SLURM_JOBID.out
python -u monitor.py $filename $timeout > monitor.$SLURM_JOBID.log &

#### replace below with the srun ####

# sleeps for 5s between updates
# this test should not trigger the monitor
# job should end normally after ~20*5s
touch $filename
for i in $(seq 1 20)
do
    echo $i
    echo $i >> $filename
    sleep 5
done
echo 'done'
