# simple monitor for Slurm jobs

## Usage

Add to the Slurm job before the `srun` line:

```
python3 -u monitor.py [file_to_monitor] [timeout_in_seconds] > monitor.$SLURM_JOBID.log &
```

This will run `scancel $SLURM_JOBID` if `[file_to_monitor]` has
not been updated in `[timeout_in_seconds]` seconds.

## Examples/tests

```
cookbg@cori09:~/src/monitor> ls
monitor.py  test1.sh  test2.sh
cookbg@cori09:~/src/monitor> for j in 1 2; do sbatch test$j.sh; done
Submitted batch job 22770056
Submitted batch job 22770063
cookbg@cori09:~/src/monitor> ls
monitor.22770056.log  monitor.py          slurm-22770063.out  test.22770056.out  test1.sh
monitor.22770063.log  slurm-22770056.out  test.22770019.out   test.22770063.out  test2.sh
cookbg@cori09:~/src/monitor> sacct -j 22770056,22770063
       JobID    JobName  Partition    Account  AllocCPUS      State ExitCode
------------ ---------- ---------- ---------- ---------- ---------- --------
22770056       test1.sh      debug      m1759        272  COMPLETED      0:0
22770056.ba+      batch                 m1759        272  COMPLETED      0:0
22770056.ex+     extern                 m1759        272  COMPLETED      0:0
22770063       test2.sh      debug      m1759        272 CANCELLED+      0:0
22770063.ba+      batch                 m1759        272  CANCELLED     0:15
22770063.ex+     extern                 m1759        272  COMPLETED      0:0
cookbg@cori09:~/src/monitor> sacct -j 22770056,22770063 -X -o elapsed
   Elapsed
----------
  00:02:31
  00:01:51
cookbg@cori09:~/src/monitor>
```