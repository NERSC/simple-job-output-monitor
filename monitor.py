#!/usr/bin/env python3

import time
import subprocess
import select
import sys
import os

filename=sys.argv[1]
timeout=int(sys.argv[2])
sleeptime=1

assert timeout > sleeptime

f = subprocess.Popen(['tail','-F',filename],\
        stdout=subprocess.PIPE,stderr=subprocess.PIPE)
p = select.poll()
p.register(f.stdout)

while p.poll(1):
    s = f.stdout.readline()

t0 = time.time()
dt = 0

while dt < timeout:
    t = time.time()
    # check if there updates and reset timer if there are
    # note: need to use readline() to flush stdout stream
    #       otherwise p.poll(1) will always return True
    if p.poll(1):
        t0 = t
        while p.poll(1):
            s = f.stdout.readline() #.decode('utf-8') #decode for printing        
    print("dt= {}".format(t-t0))
    dt = t - t0
    time.sleep(sleeptime)
else:
    print("timeout!")
    jobid = os.environ["SLURM_JOBID"]
    #cmd = "echo" # debugging
    cmd = "scancel"
    subprocess.run([cmd, jobid])
