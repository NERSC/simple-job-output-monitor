#!/bin/bash
#SBATCH -C knl
#SBATCH --nodes=1
#SBATCH --time=30
#SBATCH --qos=debug

module load python3
timeout=10 #seconds
filename=test.$SLURM_JOBID.out
python -u monitor.py $filename $timeout > monitor.$SLURM_JOBID.log &

#### replace below with the srun ####

# sleeps for increasing times between updates
# this test should trigger the monitor
touch $filename
for i in $(seq 1 30)
do
    echo $i
    echo $i >> $filename
    sleep $i
done
echo 'done' #should probably not be reached if the job is canceled...
